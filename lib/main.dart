import 'package:flutter/material.dart';
//import 'package:hello_gaes/category.dart';
import 'package:hello_gaes/category_route.dart';

//const _categoryName = 'Cake';
//const _categoryIcon = Icons.cake;
//const _categoryColor = Colors.green;

void main() {
  runApp(UnitConverterApp());
}
class UnitConverterApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Unit Converter',

//      home: Scaffold(
//        backgroundColor: Colors.green[100],
//        body: Center(
//          child: Category(
//            name: _categoryName,
//            color: _categoryColor,
//            iconLocation: _categoryIcon,
//          ),
//        ),
//      ),

      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(
          bodyColor: Colors.black,
          displayColor: Colors.grey[600],
        ),
        // This colors the [InputOutlineBorder] when it is selected
        primaryColor: Colors.grey[500],
        textSelectionHandleColor: Colors.green[500],
      ),
    home: CategoryRoute(),
    );
  }
}